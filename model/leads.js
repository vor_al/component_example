/**
 * Collection for leads
 * @type {Mongo.Collection}
 *
 */


Leads = new Mongo.Collection("leads");

LeadsProfileFieldsSchema = new SimpleSchema({
    _id: {
        type: String
    },
    name: {
        type: String
    },
    value: {
        type: String,
        optional: true
    },
    form_id: {
        type: String
    },
    language: {
        type: String
    }
});

LeadsSchema = new SimpleSchema({
    type: {
        type: String,
        allowedValues: ['form', 'chat']
    },
    status: {
        type: Number,
        allowedValues: ['void', 'not_submit', 'partially_submit', 'submitted'],
        autoValue: function() {
            if (this.isInsert && !this.isSet) {
                return 0;
            }
        }
    },
    website_id: {
        type: String
    },
    traffic_id: {
        type: String,
        autoValue: function() {
            if (!this.isSet) {
                return '';
            }
        }
    },
    session_id: {
        type: String,
        autoValue: function() {
            if (!this.isSet) {
                return '';
            }
        }
    },
    start_date: {
        type: Date,
        autoValue: function() {
            if (this.isInsert && !this.isSet) {
                return new Date;
            }
        }
    },
    submit_date: {
        type: Date,
        optional: true
    },
    conversation_duration: {
        type: Number,
        autoValue: function() {
            if (this.isInsert && !this.isSet) {
                return 0;
            }
        }
    },
    count_messages: {
        type: Number,
        autoValue: function() {
            if (this.isInsert && !this.isSet) {
                return 0;
            }
        }
    },
    profile: {
        type: [LeadsProfileFieldsSchema]
    },
    is_submit: {
        type: Boolean,
        autoValue: function() {
            if (this.isInsert && !this.isSet) {
                return false;
            }
        }
    }
});

Leads.attachSchema(LeadsSchema);

Leads.allow({
    insert: function(userId, lead) {
        return userId && lead.owner === userId;
    },
    update: function(userId, lead) {
        return userId && lead.owner === userId;
    },
    remove: function(userId, lead) {
        return userId && lead.owner === userId;
    }
});