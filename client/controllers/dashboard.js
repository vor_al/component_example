angular.module('leadomatic').controller('DashboardCtrl', ['$scope', '$reactive', '$timeout', '$modal', '$translate', '$rootScope',
    function ($scope, $reactive, $timeout, $modal, $translate, $rootScope) {

        $reactive(this).attach($scope);

        $translate('MESSAGES_LEAD_DELETED').then(function (result) {
            $scope.lead_deleted = result;
        });
        $translate('MESSAGES_SELECT_LEAD').then(function (result) {
            $scope.select_lead = result;
        });

        $scope.loadingProcess = true;
        $scope.loadingCounter = 0;
        $scope.notfirst = false;

        $scope.count1 = 0;
        $scope.count7 = 0;
        $scope.count30 = 0;
        $scope.count3m = 0;
        $scope.count12m = 0;

        $scope.tcount1 = 0;
        $scope.tcount7 = 0;
        $scope.tcount30 = 0;
        $scope.tcount3m = 0;
        $scope.tcount12m = 0;

        $scope.count = 0;
        $scope.start = 1;
        $scope.limit = 12;

        $scope.to = 0;
        $scope.filter_period = 'custom';
        $scope.filter_website = [];
        $scope.current_website = [];
        $scope.filter_location = [];
        $scope.filter_type = [];
        $scope.current_type = [];
        $scope.filter_datacount = [];
        $scope.current_datacount = [];
        $scope.searchLead = '';
        $scope.pagination_max_size = 10;

        $scope.format = 'DD/MM/YYYY';
        var now = new Date();
        $scope.filter_to = moment(now).format($scope.format);
        now.setMonth(now.getMonth() - 3);
        $scope.filter_from = moment(now).format($scope.format);

        var from_input = $("#datetimepicker_from input");
        var till_input = $("#datetimepicker_till input");
        $timeout(function() {
            if (typeof $scope.filter_from != 'undefined') {
                $("#datetimepicker_from").data("DateTimePicker").setDate($scope.filter_from);
            }
        }, 2000);
        from_input.change(function() {
            if ($scope.filter_period == 'custom') {
                $scope.filter_from = from_input.val();
                $scope.$digest();
            }
        });
        till_input.change(function() {
            if ($scope.filter_period == 'custom') {
                $scope.filter_to = till_input.val();
                $scope.$digest();
            }
        });

        $scope.error = null;
        $scope.success = null;
        $scope.checked = false;
        $scope.checked_mob = false;

        $scope.isSafariWin = false;
        $scope.isSafariMac = false;

        $scope.sort = {
            column: 'start_date',
            desc: '-1'
        };
        $scope.fields4sort = [];
        $scope.currenSortIndex = 2;

        $translate(['LEADS_STATUS', 'LEADS_TYPE', 'LEADS_INTERACTION_DATE', 'LEADS_SUBMISSION_DATE',
            'FIELDS_CONVERSATION', 'FIELDS_VISITED_PAGES', 'FIELDS_URL']).then(function (translations) {
            $scope.firstFields = {};
            $scope.firstFields[translations['LEADS_STATUS']] = 'status';
            $scope.firstFields[translations['LEADS_TYPE']] = 'type';
            $scope.firstFields[translations['LEADS_INTERACTION_DATE']] = 'start_date';
            $scope.firstFields[translations['LEADS_SUBMISSION_DATE']] = 'submit_date';

            $scope.startDateName = translations['LEADS_INTERACTION_DATE'];
            $scope.submitDateName = translations['LEADS_SUBMISSION_DATE'];

            $scope.conversationName = translations['FIELDS_CONVERSATION'];
            $scope.visitedPagesName = translations['FIELDS_VISITED_PAGES'];
            $scope.urlName = translations['FIELDS_URL'];
        });

        $translate(['PAGINATION_FIRST', 'PAGINATION_PREVIOUS', 'PAGINATION_NEXT', 'PAGINATION_LAST']).then(function (translations) {
            $scope.firstText = translations['PAGINATION_FIRST'];
            $scope.previousText = translations['PAGINATION_PREVIOUS'];
            $scope.nextText = translations['PAGINATION_NEXT'];
            $scope.lastText = translations['PAGINATION_LAST'];
        });

        $scope.doDates = function(leads, startDateName, submitDateName) {
            if (startDateName == '' || submitDateName == '') return leads;
            leads.forEach(function (lead) {
                lead.field[startDateName] = moment(lead.field[startDateName]).isValid() ?
                    moment(lead.field[startDateName]).format('DD-MM-YYYY HH:mm') : '';
                lead.field[submitDateName] = moment(lead.field[submitDateName]).isValid() ?
                    moment(lead.field[submitDateName]).format('DD-MM-YYYY HH:mm') : '';
            });

            return leads;
        };

        this.subscribe('websites', function () {
            return [];
        });

        $scope.loadLeads = function () {
            if ($scope.notfirst) $scope.loadingProcess = true;
            Meteor.call('getLeads4Website', $scope.getReactively('filter_website'), {
                    location: $scope.getReactively('filter_location'),
                    type: $scope.getReactively('filter_type'),
                    from: $scope.getReactively('filter_from'),
                    to: $scope.getReactively('filter_to'),
                    dataCount: $scope.getReactively('filter_datacount'),
                    search: $scope.getReactively('searchLead'),
                    skip: ($scope.getReactively('start') - 1) * $scope.limit,
                    limit: $scope.getReactively('limit'),
                    sort: {
                        desc: $scope.getReactively('sort.desc'),
                        column: $scope.getReactively('sort.column')
                    },
                    'fields4sort': $scope.fields4sort
                },
                $translate.use(),
                $scope.getCurrentMode(),
                function (error, result) {
                    if (error) console.log(error);
                    else {

                        $scope.count = result.count;
                        $scope.traffic_count = result.traffic_count;
                        $scope.countries = result.locations;
                        var leadsTable = $scope.doDates(result.leadsTable, result.startDateName, result.submitDateName);

                        $scope.leads = [].concat(leadsTable);
                        $scope.mob_leads = [].concat(leadsTable);

                        $scope.view_count = result.leadsTable.length;
                        $scope.translationDataCount = {
                            COUNT: $scope.count > $scope.limit ? $scope.view_count : $scope.count,
                            COUNT_ALL: $scope.count
                        };
                        $scope.translationDataCountFrom = {
                            BEGIN: 1 + ($scope.start - 1) * $scope.limit,
                            END: ($scope.start - 1) * $scope.limit + $scope.view_count
                        };
                        $scope.max_symbols_frontend_texts = result.max_symbols;

                        $scope.fields4sort = result.firstFields.concat(result.fields);
                        $scope.firstGroup = result.firstFields;

                        $scope.checked = false;
                        $scope.checkAll();
                        $scope.checked_mob = false;
                        $scope.checkAllMob();

                        $scope.$digest();
                    }
                    if ($scope.notfirst) {
                        $timeout(function() {
                            $scope.loadingProcess = false;
                        }, 500);
                    }
                    else $scope.notfirst = true;
                    $scope.loadingCounter++;
                }
            );
        };

        $scope.loadLeadsCounters = function() {
            Meteor.call('getMyLeadsLastXMonth', $scope.getCurrentMode(), function (error, result) {
                if (error) console.log(error.reason);
                else {
                    $scope.count1 = 0;
                    $scope.count7 = 0;
                    $scope.count30 = 0;
                    $scope.count3m = 0;
                    $scope.count12m = 0;

                    result.forEach(function (lead) {
                        var diff_time = moment().diff(lead.start_date, 'hours');
                        if (diff_time < 24) $scope.count1++;
                        if (diff_time < 24 * 7) $scope.count7++;
                        if (diff_time < 24 * 30) $scope.count30++;
                        if (diff_time < 24 * 30 * 3) $scope.count3m++;
                        if (diff_time < 24 * 30 * 12) $scope.count12m++;
                    });
                    $scope.$digest();
                }
                $scope.loadingCounter++;
            });
        };

        $scope.loadTrafficCounters = function () {
            Meteor.call('getMyTrafficLastPeriod2', $scope.getCurrentMode(), function (error, result) {
                if (error) console.log(error.reason);
                else {
                    $scope.tcount1 = result.count1;
                    $scope.tcount7 = result.count7;
                    $scope.tcount30 = result.count30;
                    $scope.tcount3m = result.count3m;
                    $scope.tcount12m = result.count12m;
                    $scope.$digest();
                }
                $scope.loadingCounter++;
            });
        };

        this.helpers({
            websites: function () {
                $scope.websites = Websites.find({'publish': true, 'belong_to': $scope.getCurrentMode()}).fetch();
                $scope.websites.sort(function (a, b) {
                    if (a['name'].toLowerCase() < b['name'].toLowerCase()) return -1;
                    if (a['name'].toLowerCase() > b['name'].toLowerCase()) return 1;
                    return 0;
                });
            },
            leads: function () {
                $scope.loadLeads();
            },
            count2_1: function () {
                $scope.loadLeadsCounters();
            },
            tcount1: function () {
                $scope.loadTrafficCounters();
            }
        });

        $scope.$watch('loadingCounter', function(newVal, oldVal) {
            if (newVal != oldVal && newVal == 3) {
                $timeout(function() {
                    $scope.loadingProcess = false;
                }, 1000);
            }
        });

        $scope.$watch('current_mode', function(newVal, oldVal) {
            if (newVal != oldVal && oldVal != 'undefined') {
                $scope.loadLeads();
                $scope.websites = Websites.find({'publish': true, 'belong_to': $scope.getCurrentMode()}, {sort: {'name': 1}}).fetch();
                $scope.loadLeadsCounters();
                $scope.loadTrafficCounters();
            }
        });

        $scope.findLeads = function () {
            $scope.searchLead = angular.element('#searchLead')[0].value;
            $scope.loadLeads();
        };

        $scope.checkAll = function () {
            $scope.checked = $('#check_all').prop('checked');
            $scope.leads.forEach(function (item) {
                item.checked = $scope.checked;
            });
        };

        $scope.checkAllMob = function () {
            $scope.mob_leads.forEach(function (item) {
                item.checked = $scope.checked_mob;
            });
        };

        $scope.remove = function () {
            var deleteList = [];

            $scope.leads.forEach(function (item) {
                if (item.checked === true) {
                    deleteList.push(item._id);
                }
            });

            if (deleteList.length == 0) {
                $scope.error = $scope.select_lead;
                $timeout(function () {
                    $scope.error = null;
                }, 3000);
            } else {
                var modalInstance = $modal.open({
                    templateUrl: 'client/helpers/views/delete_confirmation.ng.html',
                    windowClass: "animated fadeIn",
                    size: 'sm',
                    controller: ModalDeleteConfirmationCtrl
                });
                modalInstance.result.then(
                    function (result) {
                        if (result) {
                            Meteor.call('removeLeads', deleteList, function (error) {
                                if (error) {
                                    $scope.error = error.reason;
                                }
                                else {
                                    $scope.success = $scope.lead_deleted;
                                    $timeout(function () {
                                        $scope.success = null;
                                    }, 3000);
                                    $scope.loadLeads();
                                }
                            });
                        }
                    }, function (err) {
                        console.log('Modal dismissed at: ' + err);
                    }
                );
            }
        };

        $scope.toggleSort = function (column, index) {
            $scope.currenSortIndex = index;
            if (index == 0) $scope.sort.column = 'status';
            if (index == 1) $scope.sort.column = 'type';
            if (index == 2) $scope.sort.column = 'start_date';
            if (index > 2) $scope.sort.column = column;
            $scope.sort.desc *= -1;
        };

        $scope.changePeriodFilter = function (filterPeriod) {
            $scope.filter_period = filterPeriod;
            var now = new Date();
            switch (filterPeriod) {
                case 'TODAY':
                    $scope.filter_to = moment(now).format($scope.format);
                    $scope.filter_from = moment(now).format($scope.format);
                    break;
                case 'YESTERDAY':
                    now.setDate(now.getDate() - 1);
                    $scope.filter_to = moment(now).format($scope.format);
                    $scope.filter_from = moment(now).format($scope.format);
                    break;
                case '7_DAYS':
                    $scope.filter_to = moment(now).format($scope.format);
                    now.setDate(now.getDate() - 7);
                    $scope.filter_from = moment(now).format($scope.format);
                    break;
                case '30_DAYS':
                    $scope.filter_to = moment(now).format($scope.format);
                    now.setMonth(now.getMonth() - 1);
                    $scope.filter_from = moment(now).format($scope.format);
                    break;
            }
            $("#datetimepicker_from").data("DateTimePicker").setDate($scope.filter_from);
            $("#datetimepicker_till").data("DateTimePicker").setDate($scope.filter_to);
        };

        $scope.changeWebsiteFilter = function (filterWebsite, siteName) {
            if ($scope.filter_website.indexOf(filterWebsite) == -1) {
                $scope.filter_website.push(filterWebsite);
                $scope.current_website.push(siteName);
                $scope.loadLeads();
            }
        };

        $scope.removeWebsite = function(index) {
            $scope.filter_website.splice(index, 1);
            $scope.current_website.splice(index, 1);
            $scope.loadLeads();
        };

        $scope.changeLocationFilter = function (filterLocation) {
            if ($scope.filter_location.indexOf(filterLocation) == -1) {
                $scope.filter_location.push(filterLocation);
                $scope.loadLeads();
            }
        };

        $scope.removeLocation = function(index) {
            $scope.filter_location.splice(index, 1);
            $scope.loadLeads();
        };

        $scope.changeTypeFilter = function (filterType) {
            if ($scope.filter_type.indexOf(filterType) == -1) {
                $scope.filter_type.push(filterType);
                $translate(['FILTERS_SOURCE_CHAT', 'FILTERS_SOURCE_FORM']).then(function (translations) {
                    if (filterType == 'chat') $scope.current_type.push(translations['FILTERS_SOURCE_CHAT']);
                    if (filterType == 'form') $scope.current_type.push(translations['FILTERS_SOURCE_FORM']);
                    $scope.loadLeads();
                });
            }
        };

        $scope.removeType = function(index) {
            $scope.filter_type.splice(index, 1);
            $scope.current_type.splice(index, 1);
            $scope.loadLeads();
        };

        $scope.changeDatacountFilter = function (filterDatacount) {
            if ($scope.filter_datacount.indexOf(filterDatacount) == -1) {
                $scope.filter_datacount.push(filterDatacount);
                $translate(['FILTERS_STATUS_VOID', 'FILTERS_STATUS_NOT_SUBMIT', 'FILTERS_STATUS_PARTIAL', 'FILTERS_STATUS_SUBMITTED', 'FILTERS_STATUS_INITIATED']).then(function (translations) {
                    if (filterDatacount == 'void') $scope.current_datacount.push(translations['FILTERS_STATUS_VOID']);
                    if (filterDatacount == 'not_submit') $scope.current_datacount.push(translations['FILTERS_STATUS_NOT_SUBMIT']);
                    if (filterDatacount == 'initiated') $scope.current_datacount.push(translations['FILTERS_STATUS_INITIATED']);
                    if (filterDatacount == 'partially_submit') $scope.current_datacount.push(translations['FILTERS_STATUS_PARTIAL']);
                    if (filterDatacount == 'submitted') $scope.current_datacount.push(translations['FILTERS_STATUS_SUBMITTED']);
                    $scope.loadLeads();
                });
            }
        };

        $scope.removeDataCount = function(index) {
            $scope.filter_datacount.splice(index, 1);
            $scope.current_datacount.splice(index, 1);
            $scope.loadLeads();
        };

        $scope.jsToggle = function ($event) {
            var obj = $event.currentTarget;
            $(obj).toggleClass('active');
            $(obj).parents('tr').nextUntil('.first-data').toggleClass('active');
            return false;
        };

        $scope.fullTextToggle = function ($event) {
            var obj = $event.currentTarget;
            $(obj).hide();
            $(obj).next().show();
            return false;
        };

        $scope.owlOptions = {
            items: 1,
            nav: true,
            navText: [
                '<i class="carusel-arrow carusel-arrow_right"></i>',
                '<i class="carusel-arrow carusel-arrow_left"></i>'
            ]
        };


        function generateArray(callback) {
            Meteor.call('getLeads4Website',
                $scope.filter_website,
                {
                    location: $scope.filter_location,
                    type: $scope.filter_type,
                    from: $scope.filter_from,
                    to: $scope.filter_to,
                    dataCount: $scope.filter_datacount,
                    search: $scope.searchLead,
                    skip: 0,
                    limit: 2500,
                    sort: {
                        desc: $scope.sort.desc,
                        column: $scope.sort.column
                    },
                    'fields4sort': $scope.fields4sort
                },
                $translate.use(),
                $scope.getCurrentMode(),
                function (error, result) {
                    if (error) console.log(error.reason);
                    else {
                        var out = [], row;
                        var leads = [].concat($scope.doDates(result.leadsTable, result.startDateName, result.submitDateName));
                        var fields = [].concat(result.firstFields);
                        var visited_pages;
                        if (typeof leads[0] != 'undefiend') {
                            leads.forEach(function (lead) {
                                lead.fNames[0].forEach(function (field) {
                                    if (fields.indexOf(field) == -1)
                                        fields.push(field);
                                });
                                for (var f in lead.field) {
                                    if (!lead.field.hasOwnProperty(f)) continue;
                                    if (typeof lead.field[f] == 'object' && typeof lead.field[f][0] != 'undefined') {
                                        visited_pages = '';
                                        lead.field[f].forEach(function(page) {
                                            if (typeof page['count'] != 'undefined' && typeof page['name'] != 'undefined') {
                                                visited_pages += page['name'] + '(' + page['count'] + '); ';
                                            }
                                        });
                                        if (visited_pages != '') lead.field[f] = visited_pages;
                                    }
                                }
                            });
                            fields = fields.concat(leads[0].fNames[1]).concat(leads[0].fNames[2]);
                        }
                        out.push(fields);
                        leads.forEach(function(lead) {
                            row = [];
                            fields.forEach(function(fieldName) {
                                row.push(lead[$scope.firstFields[fieldName]] || lead.field[fieldName] || '');
                            });
                            out.push(row);
                        });
                        callback(out);
                    }
                }
            );
        }

        // FUNCTIONS FOR GENERATE XLSX FILE
        function datenum(v, date1904) {
            if(date1904) v+=1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
        }

        function sheet_from_array_of_arrays(data,    opts) {
            var ws = {};
            var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
            for(var R = 0; R != data.length; ++R) {
                for(var C = 0; C != data[R].length; ++C) {
                    if(range.s.r > R) range.s.r = R;
                    if(range.s.c > C) range.s.c = C;
                    if(range.e.r < R) range.e.r = R;
                    if(range.e.c < C) range.e.c = C;
                    var cell = {v: data[R][C] };
                    if(cell.v == null) continue;
                    var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

                    if(typeof cell.v === 'number') cell.t = 'n';
                    else if(typeof cell.v === 'boolean') cell.t = 'b';
                    else if(cell.v instanceof Date) {
                        cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                        cell.v = datenum(cell.v);
                    }
                    else cell.t = 's';

                    ws[cell_ref] = cell;
                }
            }
            if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
            return ws;
        }

        function Workbook() {
            if(!(this instanceof Workbook)) return new Workbook();
            this.SheetNames = [];
            this.Sheets = {};
        }

        function s2ab(s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        }

        // FUNCTIONS FOR GENERATE CSV FILE
        /**
         * Converts a value to a string appropriate for entry into a CSV table.  E.g., a string value will be surrounded by quotes.
         * @param {string|number|object} theValue
         * @param {string} sDelimiter The string delimiter.  Defaults to a double quote (") if omitted.
         */
        function toCsvValue(theValue, sDelimiter) {
            var t = typeof (theValue), output;

            if (typeof (sDelimiter) === "undefined" || sDelimiter === null) {
                sDelimiter = '"';
            }

            if (t === "undefined" || t === null) {
                output = "";
            } else if (t === "string") {
                output = sDelimiter + theValue + sDelimiter;
            } else {
                output = String(theValue);
            }

            return output;
        }

        /**
         * Converts an array of objects (with identical schemas) into a CSV table.
         * @param {Array} objArray An array of objects.  Each object in the array must have the same property list.
         * @param {string} sDelimiter The string delimiter.  Defaults to a double quote (") if omitted.
         * @param {string} cDelimiter The column delimiter.  Defaults to a comma (,) if omitted.
         * @return {string} The CSV equivalent of objArray.
         */
        function toCsv(objArray, sDelimiter, cDelimiter) {
            var i, l, names = [], name, value, obj, row, output = "", n, nl;

            // Initialize default parameters.
            if (typeof (sDelimiter) === "undefined" || sDelimiter === null) {
                sDelimiter = '"';
            }
            if (typeof (cDelimiter) === "undefined" || cDelimiter === null) {
                cDelimiter = ",";
            }

            for (i = 0, l = objArray.length; i < l; i++) {
                // Get the names of the properties.
                obj = objArray[i];
                row = "";
                if (i === 0) {
                    // Loop through the names
                    for (name in obj) {
                        if (obj.hasOwnProperty(name)) {
                            names.push(name);
                        }
                    }
                }

                row = "";
                for (n = 0, nl = names.length; n < nl; n += 1) {
                    name = names[n];
                    value = obj[name];
                    if (n > 0) {
                        row += cDelimiter
                    }
                    row += toCsvValue(value, sDelimiter);
                }
                output += row + "\n";
            }

            return output;
        }

        $scope.exportToXls = function () {
            if (window.isSafari) {
                $scope.loadingProcessSafari = true;
            } else {
                $scope.loadingProcess = true;
            }

            var exportToXlsCallback = function (data) {
                var date_time = new Date();
                var rand = (date_time.getMonth() + 1) + '-' + date_time.getDate() + '_' + date_time.getHours() + '-' + date_time.getMinutes() + '-' + date_time.getSeconds();


                var ws_name = "SheetJS";

                var wb = new Workbook(), ws = sheet_from_array_of_arrays(data);

                /* add worksheet to workbook */
                wb.SheetNames.push(ws_name);
                wb.Sheets[ws_name] = ws;

                var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:false, type: 'binary'});

                if (window.isSafari) {
                    Meteor.call('saveXLSX', data, rand, function (error) {
                        if (error) {
                            $scope.loadingProcess = false;
                            $scope.error = error.reason;
                            $scope.$digest();
                            console.log(error);
                        } else {
                            var path = Meteor.settings.public['uploads_dir'] + 'leads' + rand + '.xlsx';
                            $timeout(function () {
                                $scope.loadingProcess = false;
                                $scope.loadingProcessSafari = false;
                                window.downloadFileInterval(path);
                            }, 15000);
                        }
                    });
                } else {
                    saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), 'leads' + rand + '.xlsx');
                    $timeout(function () { $scope.loadingProcess = false; $scope.loadingProcessSafari = false;}, 1000);
                }
            };
            generateArray(exportToXlsCallback);
        };

        $scope.exportToCsv = function () {
            if (!window.isSafari)
                $scope.loadingProcess = true;

            var exportToCsvCallback = function (data) {
                var date_time = new Date();
                var rand = (date_time.getMonth() + 1) + '-' + date_time.getDate() + '_' + date_time.getHours() + '-' + date_time.getMinutes() + '-' + date_time.getSeconds();

                var csv = toCsv(data, '"', ',');

                if (window.isSafari) {
                    if (window.isMac) {
                        $scope.isSafariMac = true;
                    } else {
                        $scope.isSafariWin = true;
                    }
                    $timeout(function() {
                        saveAs(new Blob([csv],{type:"text/csv"}), 'leads' + rand + '.csv');
                        $scope.isSafariWin = false;
                        $scope.isSafariMac = false;
                    }, 5000);
                } else {
                    saveAs(new Blob([csv],{type:"text/csv"}), 'leads' + rand + '.csv');

                    $timeout(function () { $scope.loadingProcess = false; }, 1000);
                }
            };
            generateArray(exportToCsvCallback);
            //var data = generateArray();

        };

        $scope.exportToXml = function () {
            var exportToXmlCallback = function (leads) {
                var date_time = new Date();
                var rand = (date_time.getMonth() + 1) + '-' + date_time.getDate() + '_' + date_time.getHours() + '-' + date_time.getMinutes() + '-' + date_time.getSeconds();

                var xml = '<xml><leads>';
                var fields = leads[0];
                for (var i = 1, len = leads.length; i < len; i++) {
                    xml += '<lead>';
                    for (var num in leads[i]) {
                        if (!leads[i].hasOwnProperty(num)) continue;
                        xml += '<field name="' + fields[num] + '">' + leads[i][num].toString() + '</field>';
                    }
                    xml += '</lead>';
                }
                xml += '</leads></xml>';
                if (window.isSafari) {
                    if (window.isMac) {
                        $scope.isSafariMac = true;
                    } else {
                        $scope.isSafariWin = true;
                    }
                    $timeout(function() {
                        saveAs(new Blob([xml], {type: "text/xml"}), "leads" + rand + ".xml");
                        $scope.isSafariWin = false;
                        $scope.isSafariMac = false;
                    }, 5000);
                } else {
                    saveAs(new Blob([xml], {type: "text/xml"}), "leads" + rand + ".xml");
                }
            };
            generateArray(exportToXmlCallback);
        };
    }
]);

function doesFileExist(urlToFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();
    return !(xhr.status == "404");
}

window.downloadFileInterval = function(sUrl) {
    var attempts = 30;

    var downloadTimer = window.setInterval( function() {
        if( doesFileExist(sUrl) || (attempts == 0) ) {
            window.clearInterval( downloadTimer );
            if (attempts > 0) window.downloadFile(sUrl);
            else console.log('error loading file');
        }
        attempts--;
    }, 1000 );
};

window.downloadFile = function(sUrl) {

    //If in Chrome or Safari - download via virtual link click
    if (window.isChrome || window.isSafari) {
        //Creating new link node.
        var link = document.createElement('a');
        link.href = sUrl;

        if (link.download !== undefined){
            //Set HTML5 download attribute. This will prevent file from opening if supported.
            link.download = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
        }

        //Dispatching click event.
        if (document.createEvent) {
            var e = document.createEvent('MouseEvents');
            e.initEvent('click' ,true ,true);
            link.dispatchEvent(e);
            return true;
        }
    }

    // Force file download (whether supported by server).
    var query = '?download';

    window.open(sUrl + query, '_self');
};

window.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
window.isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1 && navigator.userAgent.toLowerCase().indexOf('chrome') == -1;
window.isMac = navigator.userAgent.toLowerCase().indexOf('mac') > -1;

function ModalDeleteConfirmationCtrl($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close(true);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}