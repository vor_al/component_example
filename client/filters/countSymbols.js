angular.module('leadomatic').filter('count_symbols', function () {
    return function (text, max_symbols) {

        if (typeof text != 'string') return text;
        if (text.length < max_symbols) return text;
        return text.substr(0, max_symbols);
    }
});
