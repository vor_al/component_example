angular.module('leadomatic').filter('amount4digit', function () {
    return function (amount) {

        var result = parseInt(amount);

        if (result > 1000000000000) result = parseInt(result / 1000000000000) + 'T';
        else if (result > 1000000000) result = parseInt(result / 1000000000) + 'G';
        else if (result > 1000000) result = parseInt(result / 1000000) + 'M';
        else if (result > 10000) result = parseInt(result / 1000) + 'K';

        return result;
    }
});
