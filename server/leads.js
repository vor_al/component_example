Meteor.publish('leads', function (options) {

    if (typeof options == 'undefined') options = {};

    Counts.publish(this, 'numbersOfLeads', Leads.find(options), {noReady: true});
    return Leads.find(options);
});

function buildOptions(websiteIds, options, mode) {
    var myWebsites;
    if (websiteIds.length > 0) {
        myWebsites = Websites.find({'publish': true, 'belong_to': mode, '_id': {'$in': websiteIds}, 'owner': Meteor.userId()}).fetch();
    } else {
        myWebsites = Websites.find({'publish': true, 'belong_to': mode, 'owner': Meteor.userId()}).fetch();
    }
    var websites_ids = [];
    var websitesNames = [];
    myWebsites.forEach(function(website) {
        websites_ids.push(website._id);
        if (website.name != null)
            websitesNames.push(website.name.replace("http://", "").replace("https://", "").replace("/", ""));
    });
    options['website_id'] = {'$in': websites_ids};

    var dataCount = options.dataCount;
    delete options.dataCount;

    var locations = options.location;
    delete options.location;

    var search = options.search;
    delete options.search;

    if (typeof options.type == 'undefined' || options.type == null || (Array.isArray(options.type) && options.type.length == 0)) {
        delete options['type'];
    } else {
        options['type'] = {'$in': options['type']};
    }

    if (typeof options.from != 'undefined' && options.from != null) {
        var date_from = moment(options.from, 'DD/MM/YYYY');
        options['start_date'] = {
            $gte: new Date(date_from)
        }
    }
    delete options['from'];

    if (typeof options.to != 'undefined' && options.to != null) {
        var date_to = moment(options.to, 'DD/MM/YYYY').add(1, 'days');
        options['start_date'] = {
            $lte: new Date(date_to)
        }
    }
    delete options['to'];

    if (date_from != null && date_to != null) {
        options['start_date'] = {
            $gte: new Date(date_from),
            $lte: new Date(date_to)
        };
    }

    var skip = 0;
    var limit = 30;
    var sort = {'start_date': -1};
    var column_filter = ['start_date', 'type', 'status', 'submit_date'];
    var sortField = {};
    var field_filter = options.fields4sort;
    delete options.fields4sort;

    if (typeof options.skip != 'undefined' && options.skip !== null) {
        skip = options.skip;
    }

    if (typeof options.limit != 'undefined' && options.limit !== null) {
        limit = options.limit;
    }
    delete options.skip;
    delete options.limit;

    if (typeof options.sort != 'undefined' && options.sort !== null) {
        if (column_filter.indexOf(options.sort.column) > -1) {
            sort = {};
            sort[options.sort.column] = options.sort.desc == '1' ? 1 : -1;
        } else if (field_filter.indexOf(options.sort.column) > -1) {
            sort = {};
            sortField = {'property': options.sort.column, 'sortOrder': options.sort.desc == '1' ? -1 : 1};
        }
    }
    delete options.sort;

    options['traffic_id'] = {'$ne': null};

    return {options: options, locations: locations, dataCount: dataCount, search: search, websitesNames: websitesNames, websites: myWebsites,
            skip: skip, limit: limit, sort: sort, sortField: sortField};
}

function getLeadsWithTraffic (leads, locations, dataCount, search, trans_statuses) {
    var leads_with_traffic = [];
    var traffic_ids = [];
    var session_ids = [];
    leads.forEach(function (lead) {
        if (lead.traffic_id != null) {
            traffic_ids.push(lead.traffic_id);
        } else if (lead.session_id != null) {
            session_ids.push(lead.session_id);
        }
    });
    var conditions = {'_id': {'$in': traffic_ids}};
    if (session_ids.length > 0) {
        conditions = {'$or': {
                '_id': {'$in': traffic_ids},
                'session_id': {'$in': session_ids}
            }
        }
    }

    var traffics = Traffic.find(conditions).fetch();

    var found = false;
    leads.forEach(function (lead) {
        if (lead.traffic_id != null || lead.session_id != null) {
            found = false;
            traffics.forEach(function (traffic) {
                if (!found && lead.traffic_id == traffic._id) {
                    lead['traffic'] = traffic;
                    found = true;
                }
            });
            if (!found) {
                traffics.forEach(function (traffic) {
                    if (!found && lead.session_id == traffic.session_id) {
                        lead['traffic'] = traffic;
                        found = true;
                    }
                });
            }
            if (found) leads_with_traffic.push(lead);
        }
    });

    var returnLocations = [];
    var leads_with_traffic_and_search = [];
    leads_with_traffic.forEach(function (lead) {
        if (returnLocations.indexOf(lead.traffic.user_info.country_name_en) == -1) {
            returnLocations.push(lead.traffic.user_info.country_name_en);
        }
        found = false;
        if (search.length > 0) {
            found = lead.type.indexOf(search) >= 0 || getStatus4Template(lead.status, trans_statuses).indexOf(search) >= 0;
            if (!found && typeof(lead.page) != 'undefined') found = lead.page.indexOf(search) >= 0;
            if (!found) {
                if (typeof lead.profile != 'undefined' && typeof lead.profile.fields != 'undefined') {
                    lead.profile.fields.forEach(function (field) {
                        if (!found && typeof field.value != 'undefined') found = field.value.indexOf(search) >= 0;
                    });
                }
            }
        }
        // search in traffic userinfo data and IP address
        if (!found) found = lead.traffic.ip.indexOf(search) >= 0 || lead.traffic.website.indexOf(search) >= 0;
        if (!found && typeof(lead.traffic.platform_type) != 'undefined') found = lead.traffic.platform_type.indexOf(search) >= 0;
        if (!found && typeof(lead.traffic.os) != 'undefined') found = lead.traffic.os.indexOf(search) >= 0;
        if (!found && typeof(lead.traffic.browser) != 'undefined') found = lead.traffic.browser.indexOf(search) >= 0;
        if (!found) for (var key in lead.traffic.user_info) {
            if (found) break;
            if (!lead.traffic.user_info.hasOwnProperty(key)) continue;
            if (typeof lead.traffic.user_info[key] == 'string' && lead.traffic.user_info[key].indexOf(search) >= 0) {
                found = true;
            }
        }
        // search in lead fields
        if (!found) {
            if (typeof lead.profile != 'undefined' && typeof lead.profile.fields != 'undefined') {
                for (var i = 0, fields_count = lead.profile.fields.length; i < fields_count; i++) {
                    if (typeof lead.profile.fields[i].value != 'undefined')
                        found = lead.profile.fields[i].value.indexOf(search) >= 0;
                    if (found) break;
                }
            }
        }
        // check using location and status filters
        if (!!found) {
            if (locations.length == 0 || (typeof lead.traffic != 'undefined' && locations.indexOf(lead.traffic.user_info.country_name_en) != -1)) {
                if (typeof lead.status != 'undefined') {
                    if (dataCount.length == 0 || dataCount.indexOf(lead.status) != -1) {
                        leads_with_traffic_and_search.push(lead);
                    }
                }
            }
        }
    });
    return {'leads': leads_with_traffic_and_search, 'locations': returnLocations};
}

function getStatus4Template(status, trans_statuses) {
    if (status == 'partially_submit') return trans_statuses['status_partial'];
    else return trans_statuses['status_' + status];
}

function leadsSortingByFieldReverse(leads, property) {
    leads.sort(function (a, b) {
        if (typeof (a['field'][property]) == 'undefined' || a['field'][property] == null) return 1;
        else if (typeof (b['field'][property]) == 'undefined' || b['field'][property] == null) return -1;
        else {
            if (a['field'][property].toLowerCase() < 'а' && b['field'][property].toLowerCase() > 'а') return -1;
            if (a['field'][property].toLowerCase() > 'а' && b['field'][property].toLowerCase() < 'а') return 1;
            if (a['field'][property].toLowerCase() < b['field'][property].toLowerCase()) return 1;
            if (a['field'][property].toLowerCase() > b['field'][property].toLowerCase()) return -1;
            return 0;
        }
    });
    return leads;
}

function leadsSortingByField(leads, sortField) {
    var property = sortField['property'];
    var sortOrder = sortField['sortOrder'];
    if (sortOrder == -1) return leadsSortingByFieldReverse(leads, property);
    leads.sort(function (a, b) {
        if (typeof (a['field'][property]) == 'undefined' || a['field'][property] == null) return 1;
        else if (typeof (b['field'][property]) == 'undefined' || b['field'][property] == null) return -1;
        else {
            if (a['field'][property].toLowerCase() < b['field'][property].toLowerCase()) return -1;
            if (a['field'][property].toLowerCase() > b['field'][property].toLowerCase()) return 1;
            return 0;
        }
    });
    return leads;
}

function getTranslations(langName) { // get all translations for fields
    var translates = TranslateFrontend.find({'group': 'fields', 'name': {'$in': ['ip', 'language', 'country', 'region', 'city',
        'platform', 'browser', 'os', 'form_data', 'website', 'url', 'conversation',
        'returning', 'referrer', 'host', 'visited_pages']}}).fetch();
    var translate_leads_data = TranslateFrontend.find({'group': 'leads', 'name': {'$in': ['submission_date', 'type', 'status', 'interaction_date']}}).fetch();

    var trans_fields = {};
    translate_leads_data.forEach(function(translate) {
        trans_fields[translate['name']] = translate[langName];
    });
    translates.forEach(function(translate) {
        trans_fields[translate['name']] = translate[langName];
    });
    return trans_fields;
}

function buildUserFields (langId) {
    var first_fields_names = ['Full Name', 'Email', 'Phone number', 'Description'];
    var fieldsDBFirst = FormFields.find({'name': {'$in': first_fields_names}}).fetch();

    var i, len;
    var user_fields = [ 'name', 'email', 'phone', 'description' ];
    fieldsDBFirst.forEach(function (f) {
        if (f['name'] == 'Full Name') {
            if (typeof f['field_names'] != 'undefined' && f['field_names'] != null) {
                for (i = 0, len = f['field_names'].length; i < len; i++) {
                    if (typeof f['field_names'][i]['language'] != 'undefined' && typeof f['field_names'][i]['value'] != 'undefined')
                        if (f['field_names'][i]['language'] == langId)
                            user_fields[0] = f['field_names'][i]['value'];
                }
            }
        }
        if (f['name'] == 'Email') {
            if (typeof f['field_names'] != 'undefined' && f['field_names'] != null) {
                for (i = 0, len = f['field_names'].length; i < len; i++) {
                    if (typeof f['field_names'][i]['language'] != 'undefined' && typeof f['field_names'][i]['value'] != 'undefined')
                        if (f['field_names'][i]['language'] == langId)
                            user_fields[1] = f['field_names'][i]['value'];
                }
            }
        }
        if (f['name'] == 'Phone number') {
            if (typeof f['field_names'] != 'undefined' && f['field_names'] != null) {
                for (i = 0, len = f['field_names'].length; i < len; i++) {
                    if (typeof f['field_names'][i]['language'] != 'undefined' && typeof f['field_names'][i]['value'] != 'undefined')
                        if (f['field_names'][i]['language'] == langId)
                            user_fields[2] = f['field_names'][i]['value'];
                }
            }
        }
        if (f['name'] == 'Description') {
            if (typeof f['field_names'] != 'undefined' && f['field_names'] != null) {
                for (i = 0, len = f['field_names'].length; i < len; i++) {
                    if (typeof f['field_names'][i]['language'] != 'undefined' && typeof f['field_names'][i]['value'] != 'undefined')
                        if (f['field_names'][i]['language'] == langId)
                            user_fields[3] = f['field_names'][i]['value'];
                }
            }
        }
    });
    return user_fields;
}

function getData4Table(leads, skip, limit, sortField, langName, trans_statuses, trans_fields, myWebsites) {

    var leads_4_table = [];

    var langs = Languages.find().fetch();
    var langId;
    langs.forEach(function(lang) {
        if (lang.name.toUpperCase() == langName.toUpperCase()) {
            langId = lang._id;
        }
    });

    var fieldsDB = FormFields.find({}).fetch();

    var fields = [trans_fields['status'], trans_fields['type'], trans_fields['interaction_date'], trans_fields['country'], trans_fields['website']];

    var fieldName = '', fieldType = '';

    var first_fields_types = ['name', 'email', 'phone', 'description'];
    var user_fields = buildUserFields(langId);

    leads.forEach(function(lead) {
        lead.field = {};

        lead.field[trans_fields['website']] = '';
        myWebsites.forEach(function (website) {
            if (lead.website_id == website._id) {
                lead.field[trans_fields['website']] = website.name.replace("http://", "").replace("https://", "").replace("/", "");
            }
        });
        lead.field[trans_fields['interaction_date']] = lead.start_date;
        lead.field[trans_fields['submission_date']] = (typeof lead.submit_date != 'undefined' && lead.is_submit) ? lead.submit_date : '';
        lead.field[trans_fields['status']] = getStatus4Template(lead.status, trans_statuses);
        lead.field[trans_fields['type']] = lead.type;
        lead.field[trans_fields['url']] = typeof lead['page'] != 'undefined' ? lead['page'] : '';

        lead.field2 = [];
        if (typeof lead.profile != 'undefined' && typeof lead.profile.fields != 'undefined') {
            lead.profile.fields.forEach(function (field) {
                fieldName = field.name;
                fieldType = '';
                fieldsDB.forEach(function (fieldDb) {
                    if (fieldName == fieldDb.name) {
                        if (typeof fieldDb.field_names != 'undefined') {
                            for (var i = 0, len = fieldDb.field_names.length; i < len; i++) {
                                if (fieldDb.field_names[i].language == langId &&
                                    typeof fieldDb.field_names[i].value != 'undefined'
                                    && fieldDb.field_names[i].value != '') {
                                    fieldName = fieldDb.field_names[i].value;
                                }
                            }
                        }
                        fieldType = fieldDb.type;
                    }
                });
                if (first_fields_types.indexOf(fieldType) == -1 || user_fields[first_fields_types.indexOf(fieldType)] != fieldName) {
                    lead.field2.push(fieldName);
                }
                
                lead.field[fieldName] = field.value;
            });
        }

        if (typeof lead.traffic != 'undefined') {
            lead.field[trans_fields['ip']] = lead.traffic.ip;
            lead.field[trans_fields['language']] = lead.traffic.user_info.language;
            lead.field[trans_fields['country']] = lead.traffic.user_info.country_name_en;
            lead.field[trans_fields['region']] = lead.traffic.user_info.region_name_en;
            lead.field[trans_fields['city']] = lead.traffic.user_info.city_name_en;

            if (typeof lead.traffic.platform_type != 'undefined') lead.field[trans_fields['platform']] = lead.traffic.platform_type;
            lead.field[trans_fields['os']] = '';
            if (typeof lead.traffic.os != 'undefined') lead.field[trans_fields['os']] += lead.traffic.os;
            if (typeof lead.traffic.os_version != 'undefined') lead.field[trans_fields['os']] += ' ' + lead.traffic.os_version;
            lead.field[trans_fields['browser']] = '';
            if (typeof lead.traffic.browser != 'undefined') lead.field[trans_fields['browser']] += lead.traffic.browser;
            if (typeof lead.traffic.browser_version != 'undefined') lead.field[trans_fields['browser']] += ' ' + lead.traffic.browser_version;

            lead.field[trans_fields['returning']] = lead.traffic.returning ? 'true' : 'false';
            lead.field[trans_fields['referrer']] = lead.traffic.user_info.referrer;
            lead.field[trans_fields['host']] = lead.traffic.user_info.host;
            lead.field[trans_fields['visited_pages']] = lead.traffic.pages_visited;
        }

        var form_data = '';
        if (typeof lead.form_view == 'object') {
            for (var value in lead.form_view) {
                if (!lead.form_view.hasOwnProperty(value)) continue;
                if (lead.form_view[value] != '') {
                    form_data += ', ' + lead.form_view[value];
                }
            }
            if (form_data[0] == ',') form_data = form_data.substr(2);
        }
        lead.field[trans_fields['form_data']] = form_data;
        if (lead.type == 'form' && form_data != '') {
            lead.field2.push(trans_fields['form_data']);
        }

        var chat = {};
        if (lead.type == 'chat') {
            chat = Chats.findOne({'lead_id': lead._id});
            if (typeof chat != 'undefined' && typeof chat._id != 'undefined') lead.field2.push(trans_fields['conversation']);
        }

        leads_4_table.push({
            '_id': lead._id,
            'field': lead.field,
            'field2': lead.field2,
            'website_id': lead.website_id,
            'chat_id': (lead['type'] == 'form' || typeof chat == 'undefined' || typeof chat._id == 'undefined') ? '' : chat._id
        });
    });

    if (typeof sortField['property'] != 'undefined' && typeof sortField['sortOrder'] != 'undefined') {
        leads_4_table = leadsSortingByField(leads_4_table, sortField);
    }
    leads_4_table = leads_4_table.slice(skip, skip + limit);

    return {'table': leads_4_table, 'fields': fields.concat(user_fields)};
}

function removeEmptyFields (leads, fields, trans_fields) {

    var traffic_fields1 = [
        trans_fields['ip'],
        trans_fields['submission_date'],
        trans_fields['language'],
        trans_fields['region'],
        trans_fields['city'],
        trans_fields['platform'],
        trans_fields['os'],
        trans_fields['browser'],
        trans_fields['url']
    ];
    var traffic_fields2 = [
        trans_fields['referrer'],
        trans_fields['returning'],
        trans_fields['host'],
        trans_fields['visited_pages']
    ];

    var firstFields = fields;

    leads.forEach(function(lead) {
        lead.fNames = [];
        if (lead.field2.length > 0) lead.fNames.push(lead.field2);
        lead.fNames.push(traffic_fields1);
        lead.fNames.push(traffic_fields2);

        lead.fNamesMob = [];
        var allFields = fields;

        if (lead.field2.length > 0) allFields.concat(lead.field2);
        allFields = allFields.concat(traffic_fields1).concat(traffic_fields2);

        for (var i = 0, len = allFields.length; i < len; i++) {
            if (typeof lead.fNamesMob[i / 5 >> 0] == 'undefined') lead.fNamesMob[i / 5 >> 0] = [];
            lead.fNamesMob[i / 5 >> 0].push(allFields[i]);
        }
    });

    return {
        leads: leads,
        firstFields: firstFields
    };
}

Meteor.methods({

    'getLeads4Website': function (websiteIds, options, langName, mode) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('access denied');
        }
        var from = options.from;
        var to = options.to;

        var builtOptions = buildOptions(websiteIds, options, mode);
        options = builtOptions['options'];

        var locations = builtOptions['locations'];
        var dataCount = builtOptions['dataCount'];
        var search = builtOptions['search'];
        var myWebsites = builtOptions['websites'];
        var skip = builtOptions['skip'];
        var limit = builtOptions['limit'];
        var sort = builtOptions['sort'];
        var sortField = builtOptions['sortField'];

        var leads = Leads.find(options, {sort: sort}).fetch();

        var statuses = TranslateFrontend.find({'group': 'filters', 'name': {'$in': ['status_void', 'status_not_submit', 'status_partial', 'status_submitted', 'status_initiated']}}).fetch();
        var trans_statuses = {};
        statuses.forEach(function(translate) {trans_statuses[translate['name']] = translate[langName]; });

        var result = getLeadsWithTraffic(leads, locations, dataCount, search, trans_statuses);
        var returnLocations = result.locations; // countries for filter

        var count = result.leads.length;
        var traffic_count = 0;
        var max_symbols = 0;
        var interaction_date = '';
        var submission_date = '';
        var finalResult = {leads: [], firstFields: [], firstFieldsMob: []};

        if (mode == 'customer') {

            var trans_fields = getTranslations(langName);
            interaction_date = trans_fields['interaction_date'];
            submission_date = trans_fields['submission_date'];
            var result2 = getData4Table(result.leads, skip, limit, sortField, langName, trans_statuses, trans_fields, myWebsites);

            finalResult = removeEmptyFields(result2.table, result2.fields, trans_fields);

            var config = Configuration.find({}).fetch();
            max_symbols = typeof config[0]['max_symbols_frontend_texts'] != 'undefined' ? config[0]['max_symbols_frontend_texts'] : 100;
        } else if (mode == 'publisher') {
            traffic_count = Traffic.find(
                {
                    'created': {
                        '$gte': new Date(moment(from, "DD/MM/YYYY").format('YYYY-MM-DD HH:mm:ss.ms')),
                        '$lte': new Date(moment(to, "DD/MM/YYYY").format('YYYY-MM-DD HH:mm:ss.ms'))
                    },
                    'website': {'$in': builtOptions['websitesNames']}
                },
                {'fields': {'_id': 1}}
            ).count();
        }

        return {
            'leadsTable': finalResult.leads,
            'firstFields': finalResult.firstFields,
            'locations': returnLocations,
            'count': count,
            'traffic_count': traffic_count,
            'max_symbols': max_symbols,
            'startDateName': interaction_date,
            'submitDateName': submission_date
        };
    },

    'getMyLeadsLastXMonth': function (mode) {
        var websites = Websites.find({'publish': true, 'belong_to': mode, 'owner': Meteor.userId()}).fetch();
        var websites_ids = [];
        websites.forEach(function(website){
            websites_ids.push(website._id);
        });

        var now = new Date();
        now.setMonth(now.getMonth() - 12);
        var leads = Leads.find(
            {
                'start_date': {'$gte': new Date(moment(now).format('YYYY-MM-DD HH:mm:ss.ms'))},
                'website_id': {'$in': websites_ids},
                'traffic_id': {'$ne': null},
                'session_id': {'$ne': null},
                'status': {'$ne': 'void'}
            }
            ,
            {'fields': {'traffic_id': 1, 'start_date': 1, 'website_id': 1}, 'sort': {'start_date': 1}}
        ).fetch();

        var traffic_ids = [];
        leads.forEach(function(lead) {
            if (traffic_ids.indexOf(lead['traffic_id']) == -1)
                traffic_ids.push(lead['traffic_id']);
        });
        var traffics = Traffic.find({'_id': {'$in': traffic_ids}}, {'fields': {'_id': 1}}).fetch();
        var traffic_ids_real = [];
        traffics.forEach(function(t) {
            if (traffic_ids_real.indexOf(t['_id']) == -1)
                traffic_ids_real.push(t['_id']);
        });

        if (traffic_ids.length != traffic_ids_real.length) {
            var leads_real = [];
            leads.forEach(function(lead) {
                if (traffic_ids_real.indexOf(lead.traffic_id) != -1)
                    leads_real.push(lead);
            });
            return leads_real;
        } else return leads;
    },

    'removeLeads': function (ids) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('access denied');
        }
        Leads.remove({_id: {$in: ids}});
    },

    'saveXLSX': function (wb, rand) {
        var path = Meteor.settings.public['uploads_full_path'];
        var name = 'leads' + rand + '.xlsx';

        var workbook = ExcelBuilder.createWorkbook(path, name);
        var sheet1 = workbook.createSheet('sheet1', wb[0].length, wb.length);

        for(var R = 0; R < wb.length; ++R) {
            if (typeof wb[R] == 'undefined') continue;
            for(var j = 0; j < wb[R].length; ++j) {
                sheet1.set(j + 1, R + 1, typeof wb[R][j] == 'undefined' ? '' : wb[R][j]);
            }
        }

        // Save it
        workbook.save(function(error, result) {
            workbook.cancel();
            if (error) {
                console.log('Error: ' + error);
            } else {
                console.log('congratulations, your workbook created');
                return result;
            }
        });
    },

    'saveCSV': function (data, rand) {
        var uploads_dir = Meteor.settings.public['uploads_full_path'];
        var name = 'leads' + rand + '.csv';
        var fs = Meteor.npmRequire('fs');
        return fs.writeFileSync(uploads_dir + name, data);
    }
});
